import HasRoleDirective from './hasRoleDirective';

export default {
  hasRole: HasRoleDirective
};
