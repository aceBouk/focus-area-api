import { SchemaDirectiveVisitor } from 'apollo-server-express';
import {
  GraphQLDirective,
  DirectiveLocation,
  GraphQLList,
  defaultFieldResolver
} from 'graphql';
import { ensureAuthenticated } from '../controllers/authController';
import { AuthorizationError } from '../errors/authError';

class HasRoleDirective extends SchemaDirectiveVisitor {
  static getDirectiveDeclaration(directiveName, schema) {
    return new GraphQLDirective({
      name: 'hasRole',
      locations: [DirectiveLocation.FIELD_DEFINITION, DirectiveLocation.QUERY],
      args: {
        roles: {
          type: new GraphQLList(schema.getType('Roles'))
        }
      }
    });
  }

  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;
    const roles = this.args.roles;
    field.resolve = async(...args) => {
      const [, , { req }] = args;
      try {
        const user = await ensureAuthenticated(req);
        const userRole = user.role;
        if (roles.some(role => role === userRole.toUpperCase())) {
          const result = await resolve.apply(this, args);
          return result;
        }
        throw new AuthorizationError({
          data: {
            something: 'important'
          },
          internalData: {
            error: `The SQL server died.`
          }
        });
      } catch (e) {
        throw new AuthorizationError({
          data: {
            something: 'important'
          },
          internalData: {
            error: `The SQL server died.`
          }
        });
      }
    };
  }
}

export default HasRoleDirective;
