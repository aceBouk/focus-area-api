const createCoachingFramework = async({ input }, { req, models }) => {
  try {
    const { dataValues } = await models.CoachingFramework.create({title: input.title, organizationId: input.organizationId});
    return { coachingFramework: dataValues };
  } catch (e) {
    console.log(e);
  }
};

export default {
  createCoachingFramework
};
