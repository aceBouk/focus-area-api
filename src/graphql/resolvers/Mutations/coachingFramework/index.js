import coachingFrameworkMutation from './coachingFrameworkMutations';

export default () => ({
  ...coachingFrameworkMutation
});
