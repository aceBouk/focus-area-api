
const getCoachingFramework = async({ input }, { models }) => {
  try {
    const { dataValues } = await models.CoachingFramework.findOne({
      where: {
        organizationId: input.organizationId
      }
    });
    return { coachingFramework: dataValues };
  } catch (e) {
    console.log(e);
  }
};

export default {
  getCoachingFramework
};
