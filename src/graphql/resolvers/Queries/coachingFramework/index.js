import coachingFrameworkQueries from './coachingFrameworkQueries';

export default () => ({
  ...coachingFrameworkQueries
});
