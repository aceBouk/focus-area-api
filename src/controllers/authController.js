import jwt from 'jsonwebtoken';

export const ensureAuthenticated = async req => {
  const token = req.headers['x-access-token'];
  if (token) {
    try {
      const decodedToken = await jwt.verify(token, process.env.SECRET_PHRASE);
      return decodedToken;
    } catch (error) {
      throw error;
    }
  }
};
