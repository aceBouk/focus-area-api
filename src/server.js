import express from 'express';
import morgan from 'morgan';
import compression from 'compression';
import bodyParser from 'body-parser';
import { bodyParserGraphQL } from 'body-parser-graphql';
import { formatError } from 'apollo-errors';
import timeout from 'connect-timeout';
import { ApolloServer, makeExecutableSchema } from 'apollo-server-express';
import http from 'http';
import uuidv1 from 'uuid/v1';
import { importSchema } from 'graphql-import';
import resolvers from './graphql/resolvers/';
import models from './models';
import schemaDirectives from './directives';

require('dotenv').config();

const TARGET = process.env.npm_lifecycle_event;

const typeDefs = importSchema('src/graphql/schemas/schema.graphql');
const app = express();

if (TARGET === 'dev') {
  app.use(morgan('dev'));
} else {
  const shouldCompress = (req, res) => {
    if (req.headers['x-no-compression']) {
      return false;
    }
    return compression.filter(req, res);
  };
  const haltOnTimedout = (req, res, next) => {
    if (!req.timedout) next();
  };
  app.use(timeout('10s'));
  app.use(compression({ filter: shouldCompress }));
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(bodyParserGraphQL());
  app.use(haltOnTimedout);

  app.use((req, res, next) => {
    req.id = uuidv1();
    next();
  });
}

const playground = (env) => {
  return env === 'dev' ? {
    settings: {
      'editor.theme': 'dark'
    }
  } : false;
};

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
  resolverValidationOptions: { requireResolversForResolveType: false },
  schemaDirectives

});

const server = new ApolloServer({
  schema,
  context: ({req}) => ({
    req,
    models
  }),
  formatError,
  playground: playground(TARGET)
});

server.applyMiddleware({ app });

const httpServer = http.createServer(app);

models.sequelize.sync({}).then(() => {
  httpServer.listen({ port: process.env.PORT }, () => {
    if (TARGET === 'dev') {
      console.log(`🚀 Server ready at http://localhost:${process.env.PORT}${server.graphqlPath}`);
    }
  });
});
