import uuid from 'uuid/v4';

export default (sequelize, DataTypes) => {
  const FocusArea = sequelize.define('focus_area', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: uuid()
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    status: DataTypes.ENUM('inactive', 'active'),
    labels: DataTypes.ARRAY(DataTypes.STRING),
    focus_area_category_id: DataTypes.STRING
  });

  // FocusArea.associate = (models) => {
  //   FocusArea.hasMany(models.FocusAreaContent, {as: 'FocusAreaContent'});
  //   FocusArea.hasMany(models.CoacheeFocusArea, {as: 'CoacheeFocusArea'});
  // };
  return FocusArea;
};
