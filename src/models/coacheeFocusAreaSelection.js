import uuid from 'uuid/v4';

export default (sequelize, DataTypes) => {
  const CoacheeFocusAreaSelection = sequelize.define('coachee_focus_area', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: uuid()
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    coacheeId: {
      type: DataTypes.STRING,
      unique: true
    },
    focusAreaId: {
      type: DataTypes.STRING,
      unique: true
    }
  });

  return CoacheeFocusAreaSelection;
};
