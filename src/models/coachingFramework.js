import uuid from 'uuid/v4';

export default (sequelize, DataTypes) => {
  const CoachingFramework = sequelize.define('coaching_framework', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: uuid()
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    title: {
      type: DataTypes.STRING,
      unique: true
    },
    organizationId: {
      type: DataTypes.STRING,
      unique: true
    }
  });

  CoachingFramework.associate = (models) => {
    CoachingFramework.hasMany(models.FocusArea, {as: 'FocusArea'});
  };

  return CoachingFramework;
};
