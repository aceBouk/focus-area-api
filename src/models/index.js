import Sequelize from 'sequelize';

const sequelize = new Sequelize('test', 'postgres', '', {
  dialect: 'postgres',
  logging: process.env.npm_lifecycle_event === 'dev',
  define: {
    underscore: true
  }
});

const models = {
  CoachingFramework: sequelize.import('./coachingFramework'),
  FocusArea: sequelize.import('./focusArea'),
  FocusAreaCategory: sequelize.import('./focusAreaCategory'),
  FocusAreaContent: sequelize.import('./focusAreaContent'),
  FocusAreaCategoryContent: sequelize.import('./focusAreaCategoryContent'),
  CoacheeFocusAreaSelection: sequelize.import('./coacheeFocusAreaSelection.js')
};

Object.keys(models).forEach((modelName) => {
  if ('associate' in models[modelName]) {
    models[modelName].associate(models);
  }
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

export default models;
