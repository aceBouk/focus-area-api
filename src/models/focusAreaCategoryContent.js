import uuid from 'uuid/v4';

export default (sequelize, DataTypes) => {
  const FocusAreaCategoryContent = sequelize.define('focus_area_category_content', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: uuid()
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    status: DataTypes.ENUM('draft', 'publish', 'unpublish'),
    title: DataTypes.STRING,
    definition: DataTypes.STRING,
    category_image: DataTypes.STRING,
    locale: DataTypes.ARRAY(DataTypes.STRING)
  });

  return FocusAreaCategoryContent;
};
