import uuid from 'uuid/v4';

export default (sequelize, DataTypes) => {
  const FocusAreaContent = sequelize.define('focus_area_content', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: uuid()
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    status: DataTypes.ENUM('draft', 'publish', 'unpublish'),
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    headerImage: DataTypes.STRING,
    icon: DataTypes.STRING,
    locale: DataTypes.ARRAY(DataTypes.STRING)
  });

  return FocusAreaContent;
};
